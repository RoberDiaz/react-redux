import React from 'react';
// ya no usamos fragment, se coloca Router en su lugar
//import React, { Fragment } from 'react';
import Header from './components/Header';
import Productos from "./components/Productos";
import NuevoProducto from "./components/NuevoProducto";

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
//import './App.css'; 

function App() {
  return (
    
         <Router>
           <Header/>
             <div className="container">
               <Switch>
                  <Route exact path="/" component={Productos}/>
                  <Route exact path="/productos/nuevo" component={NuevoProducto}/>
               </Switch>
             </div>
    
         </Router>
  );
}

export default App;
