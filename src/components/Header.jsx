import React from 'react';
import { Link } from 'react-router-dom';

const Header = ()=>{
    return(
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary justify-content-between" >
            <div className="container">
                <h1 className="">
                   <Link to={'/'} className="returner"> Crud con React, Redux, REST API & Axios</Link>    
                </h1>
                <a className="btn d-md-inline-block d-block  btn-danger" href="/productos/nuevo">Agregar producto &#43;</a>
            </div>
        </nav>
    )
}
export default Header;